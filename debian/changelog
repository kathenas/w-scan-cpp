w-scan-cpp (20231015+dfsg-3) unstable; urgency=medium

  * 'd/control':
    - Remove version requirement on 'librepfunc-dev' Build-Depends.
    - Update obsolete pkg-conf to pkgconf in Build-Depends.

 -- Phil Wyett <philip.wyett@kathenas.org>  Fri, 23 Feb 2024 07:26:29 +0000

w-scan-cpp (20231015+dfsg-2) unstable; urgency=medium

  * Fix broken 'd/watch' file and only use sources available from GitHub.
  * Remove '0~' prefix at beginning of package version.

 -- Phil Wyett <philip.wyett@kathenas.org>  Wed, 24 Jan 2024 17:05:05 +0000

w-scan-cpp (0~20231015+dfsg-1) unstable; urgency=medium

  * New maintainer (Closes: #1052444).
  * Disable verbose in 'd/rules'.
  * Changed VCS field data now repo has moved.
  * Remove non-DFSG sources 'doc/service_list.dtd' and 'doc/services.xml'.
  * Add upstream contact to 'd/copyright'.

  [ Fab Stz ]
  * New upstream version 0~20231015

 -- Phil Wyett <philip.wyett@kathenas.org>  Tue, 16 Jan 2024 17:10:39 +0100

w-scan-cpp (0~20230604-1) unstable; urgency=medium

  * remove 'LCN' patches, it is now supported upstream
  * d/control replace obsolete build-deps
  * New upstream version 0~20230604
  * d/patch/101-makefile_verbose: use CC & CXX from environment if available

 -- Fab Stz <fabstz-it@yahoo.fr>  Tue, 20 Jun 2023 22:05:45 +0200

w-scan-cpp (0~20230121-1) unstable; urgency=medium

  * Bump Standards-Version to 4.6.2 (no change needed)
  * New upstream version 0~20230121
  * d/copyright: update to match latest upstream
  * remove patches that were merged upstream
  * install CONTRIBUTORS file

 -- Fab Stz <fabstz-it@yahoo.fr>  Mon, 30 Jan 2023 20:45:32 +0100

w-scan-cpp (0~20220105-1) unstable; urgency=medium

  * Initial release. (Closes: #1023169)

 -- Fab Stz <fabstz-it@yahoo.fr>  Tue, 20 Dec 2022 22:33:03 +0100
